﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
    public float speed;
    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }
}
